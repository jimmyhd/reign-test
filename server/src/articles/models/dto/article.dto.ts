import { IStoryDocument } from './../interfaces/article.interface';
export class ArticleDto {
	query: string;
	articles: IStoryDocument[];
}
