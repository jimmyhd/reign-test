import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IArticle } from './../model/interfaces/article.interface';
import { Paths } from './../model/static/paths';

@Injectable({ providedIn: 'root' })
export class ArticleService {
	constructor(private http: HttpClient) {}

	getStory(): Observable<IArticle[]> {
		return this.http.get<IArticle[]>(Paths.GET_STORYS);
	}

	deleteStory(idStory: number): Observable<boolean> {
		return this.http.delete<boolean>(Paths.PUT_DELETE_STORY + '/' + idStory);
	}
}
