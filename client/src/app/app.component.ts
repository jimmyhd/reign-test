import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { IArticleComponent } from './model/interfaces/article.interface';
import { ArticleService } from './services/article.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
	constructor(private articleService: ArticleService) {}

	listSuscription: Subscription | undefined;
	deleteSuscription: Subscription | undefined;

	listStory: IArticleComponent[] = [];

	ngOnInit(): void {
		this.listSuscription = this.articleService.getStory().subscribe((data) => {
			this.listStory = data
				.filter((item) => item.storyTitle || item.title)
				.map((story) => {
					let title = story.storyTitle;
					if (!title) {
						title = story.title;
					}
					const dataComponent: IArticleComponent = {
						id: story.id,
						author: story.author,
						publishDate: story.publishDate,
						title: title,
						url: story.url
					};

					return dataComponent;
				});
		});
	}

	deleteStory(idStory: number) {
		this.articleService.deleteStory(idStory).subscribe((data) => {
			if (data && data == true) {
				this.listStory = this.listStory.filter((item) => item.id !== idStory);
			}
		});
	}

	trackByItem(index: number, item: IArticleComponent) {
		return item.id;
	}

	ngOnDestroy(): void {
		this.listSuscription?.unsubscribe();
		this.deleteSuscription?.unsubscribe();
	}
}
