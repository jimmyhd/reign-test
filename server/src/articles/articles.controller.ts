import { Controller, Delete, Get, Param } from '@nestjs/common';
import { IStoryDocument } from './models/interfaces/article.interface';
import { ArticlesService } from './services/articles/articles.service';

@Controller('articles')
export class ArticlesController {
	constructor(private articleService: ArticlesService) {}

	@Get('/story')
	async getAllArticles(): Promise<IStoryDocument[]> {
		const article = await this.articleService.getArticle();

		if (article) {
			return article[0].articles;
		}

		return [];
	}

	@Delete('/delete/:idStory')
	async deleteStory(@Param('idStory') idStory: number): Promise<boolean> {
		const story = await this.articleService.deleteStory(idStory);
		return story ? true : false;
	}
}
