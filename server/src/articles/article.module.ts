import { HttpModule, Module } from '@nestjs/common';
import { MongoModule } from './../db/mongo.module';
import { ArticlesController } from './articles.controller';
import { ArticlesPublishService } from './services/articles/articles-publish.service';
import { ArticlesService } from './services/articles/articles.service';
import { TaskService } from './services/task/task.service';

@Module({
	imports: [MongoModule, HttpModule],
	controllers: [ArticlesController],
	providers: [ArticlesService, ArticlesPublishService, TaskService]
})
export class ArticleModule {}
