import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IArticleDocument } from '../../models/interfaces/article.interface';
import { ArticleDto } from './../../models/dto/article.dto';
import { IStoryDocument } from './../../models/interfaces/article.interface';
@Injectable()
export class ArticlesService {
	constructor(@InjectModel('Articles') private readonly articleModel: Model<IArticleDocument>) {}

	addStory(data: IStoryDocument): void {
		this.articleModel.updateMany({ query: 'nodejs' }, { $push: { articles: data } });
	}

	updateStory(data: IStoryDocument): void {
		this.articleModel.findOneAndUpdate(
			{ 'articles.id': data.id },
			{
				$set: {
					'articles.$.storyTitle': data.id,
					'articles.$.title': data.title,
					'articles.$.url': data.url,
					'articles.$.author': data.author,
					'articles.$.publishDate': data.publishDate
				}
			},
			{ new: true, upsert: true }
		);
	}

	async existsStory(idStory: number): Promise<boolean> {
		return this.articleModel.exists({ 'articles.id': idStory });
	}

	async existsArticle(): Promise<boolean> {
		return this.articleModel.exists({ query: 'nodejs' });
	}

	async createArticle(article: ArticleDto): Promise<IArticleDocument> {
		const createArticle = new this.articleModel(article);
		return createArticle.save();
	}

	async getArticle(): Promise<IArticleDocument[]> {
		return await this.articleModel.aggregate([
			{
				$match: {
					articles: {
						$elemMatch: {
							$and: [{ state: 1 }]
						}
					}
				}
			},
			{
				$project: {
					articles: {
						$filter: {
							input: '$articles',
							as: 'articles',
							cond: {
								$and: [{ $eq: ['$$articles.state', 1] }]
							}
						}
					}
				}
			}
		]);
	}

	deleteStory(id: number): Promise<IArticleDocument> {
		return this.articleModel
			.findOneAndUpdate(
				{ 'articles.id': id },
				{
					$set: {
						'articles.$.state': 0
					}
				},
				{ new: true, upsert: true }
			)
			.exec();
	}
}
