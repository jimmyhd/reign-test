import { TestBed } from '@angular/core/testing';

import { ArticleInterceptor } from './article.interceptor';

describe('ArticleInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      ArticleInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: ArticleInterceptor = TestBed.inject(ArticleInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
