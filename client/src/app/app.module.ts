import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ArticleInterceptor } from './article.interceptor';
import { CustomDatePipe } from './pipe/custom-date.pipe';
import { ArticleService } from './services/article.service';
@NgModule({
	declarations: [AppComponent, CustomDatePipe],
	imports: [BrowserModule, AppRoutingModule, CommonModule, HttpClientModule],
	providers: [
		ArticleService,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: ArticleInterceptor,
			multi: true
		}
	],
	bootstrap: [AppComponent]
})
export class AppModule {}
