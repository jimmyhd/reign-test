import * as mongoose from 'mongoose';

export const ArticleSchema = new mongoose.Schema({
	query: String,
	articles: [
		{
			id: Number,
			storyTitle: String,
			title: String,
			url: String,
			author: String,
			publishDate: String,
			state: Number
		}
	]
});
