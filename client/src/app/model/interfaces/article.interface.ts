export interface IArticle {
	id: number;
	storyTitle: string;
	title: string;
	url: string;
	author: string;
	publishDate: string;
	state: number;
}

export interface IArticleComponent {
	id: number;
	title: string;
	url: string;
	author: string;
	publishDate: string;
}
