import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { ArticleModule } from './articles/article.module';

@Module({
	imports: [ScheduleModule.forRoot(), ArticleModule]
})
export class AppModule {}
