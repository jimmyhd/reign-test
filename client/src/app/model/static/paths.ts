import { environment } from './../../../environments/environment';
export class Paths {
	public static readonly GET_STORYS = environment.hots + '/articles/story';
	public static readonly PUT_DELETE_STORY = environment.hots + '/articles/delete';
}
