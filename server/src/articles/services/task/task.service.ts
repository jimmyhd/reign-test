import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { ArticleDto } from './../../models/dto/article.dto';
import { IStoryDocument } from './../../models/interfaces/article.interface';
import { ArticlesPublishService } from './../articles/articles-publish.service';
import { ArticlesService } from './../articles/articles.service';

@Injectable()
export class TaskService {
	private readonly logger = new Logger(TaskService.name);
	constructor(
		private articlesPublishService: ArticlesPublishService,
		private articleService: ArticlesService
	) {}

	@Cron('0 * * * *')
	// @Cron('5 * * * * *')
	handleCron() {
		this.articlesPublishService.getPublishedArticles().subscribe(
			async (data) => {
				if (data) {
					this.saveUpdateArticle(data);
				} else {
					this.logger.warn('The API does not return any value');
				}
			},
			(error) => this.logger.error('Ups! An error has occurred: ', error)
		);
	}

	private async saveUpdateArticle(data: IStoryDocument[]) {
		const exists = await this.articleService.existsArticle();

		if (exists) {
			console.log('*********EXISTS*********');

			data.forEach(async (item) => {
				const existsStory = await this.articleService.existsStory(item.id);
				if (existsStory) {
					console.log('*********UPDATE*********');
					this.articleService.updateStory(item);
					console.log('*********FIN UPDATE*********');
				} else {
					console.log('*********NEW*********');
					this.articleService.addStory(item);
					console.log('*********FIN NEW*********');
				}
			});
		} else {
			const sendData: ArticleDto = { query: 'nodejs', articles: data };
			const request = await this.articleService.createArticle(sendData);
			if (request) {
				this.logger.log('The information was saved correctly');
			} else {
				this.logger.error('Ups! An error has occurred. ');
			}
		}
	}
}
