import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'customDate'
})
export class CustomDatePipe implements PipeTransform {
	transform(value: string): string {
		return this.calculateDate(value);
	}

	private calculateDate(date: string): string {
		const dateNow = new Date();
		const dateTransform = new Date(date);

		let request = this.compareDDMMYYY(dateTransform, dateNow, 'equalsDay');
		if (request) {
			return new DatePipe('en-US').transform(dateTransform.toISOString(), 'h:mm a', 'GMT')!;
		}

		request = this.compareDDMMYYY(dateTransform, dateNow, 'yesterday');
		if (request) {
			return 'Yesterday';
		}

		request = this.compareDDMMYYY(dateTransform, dateNow, 'year');
		if (request) {
			return new DatePipe('en-US').transform(dateTransform, 'MMM d')!;
		}

		return new DatePipe('en-US').transform(dateTransform, 'yyyy/MM/d')!;
	}

	private compareDDMMYYY(date: Date, dateNow: Date, type: TypeValidate): boolean {
		const dateEvaluate = new Date(date.getFullYear(), date.getMonth(), date.getDay());
		const dateNowCustom = new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDay());

		switch (type) {
			case 'equalsDay':
				return dateEvaluate.valueOf() === dateNowCustom.valueOf();
			case 'yesterday':
				const dateYesterday = new Date(
					dateNow.getFullYear(),
					dateNow.getMonth(),
					dateNow.getDay() - 1
				);
				return dateEvaluate.valueOf() === dateYesterday.valueOf();

			case 'year':
				return dateEvaluate.getFullYear() === dateNowCustom.getFullYear();
		}
	}
}

type TypeValidate = 'equalsDay' | 'yesterday' | 'year';
