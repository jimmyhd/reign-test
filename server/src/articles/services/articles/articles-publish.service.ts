import { HttpService, Injectable } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IArticle, IStoryDocument } from './../../models/interfaces/article.interface';
@Injectable()
export class ArticlesPublishService {
	constructor(private httpService: HttpService) {}

	getPublishedArticles(): Observable<IStoryDocument[]> {
		return this.httpService.get('http://hn.algolia.com/api/v1/search_by_date?query=nodejs').pipe(
			map((axiosResponse: AxiosResponse) => {
				const data: IArticle = axiosResponse.data;
				return data.hits.map((element) => {
					const storyDocument: IStoryDocument = {
						id: element.objectID,
						storyTitle: element.story_title,
						title: element.title,
						url: element.story_url,
						author: element.author,
						publishDate: element.created_at,
						state: 1
					};
					return storyDocument;
				});
			})
		);
	}
}
