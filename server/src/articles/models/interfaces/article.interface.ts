import { Document } from 'mongoose';

export interface IArticleDocument extends Document {
	query: string;
	articles: IStoryDocument[];
}

export interface IStoryDocument {
	id: number;
	storyTitle: string;
	title: string;
	url: string;
	author: string;
	publishDate: string;
	state: number;
}

export interface IArticle {
	hits: IHit[];
	nbHits: number;
	page: number;
	nbPages: number;
	hitsPerPage: number;
	exhaustiveNbHits: boolean;
	query: string;
	params: string;
	processingTimeMS: number;
}

export interface IHit {
	created_at: string;
	title?: string;
	url?: string;
	author: string;
	points?: number;
	story_text?: string;
	comment_text?: string;
	num_comments?: number;
	story_id?: number;
	story_title?: string;
	story_url?: string;
	parent_id?: number;
	created_at_i: number;
	_tags: string[];
	objectID: number;
	_highlightResult: IHighlightResult;
}

export interface IHighlightResult {
	title?: ITitle;
	author: ITitle;
	story_text?: IStorytext;
	comment_text?: IStorytext;
	story_title?: ITitle;
	story_url?: IStoryurl;
	url?: IStorytext;
}

export interface IStoryurl {
	value: string;
	matchLevel: string;
	matchedWords: string[];
	fullyHighlighted?: boolean;
}

export interface IStorytext {
	value: string;
	matchLevel: string;
	fullyHighlighted: boolean;
	matchedWords: string[];
}

export interface ITitle {
	value: string;
	matchLevel: string;
	matchedWords: any[];
}
