import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ArticlesService } from './../articles/services/articles/articles.service';
import { ArticleSchema } from './article.schema';

@Module({
	imports: [
		MongooseModule.forRoot(process.env.DB_URL, { useNewUrlParser: true }),
		MongooseModule.forFeature([{ name: 'Articles', schema: ArticleSchema }])
	],
	exports: [MongooseModule],
	providers: [ArticlesService]
})
export class MongoModule {}
